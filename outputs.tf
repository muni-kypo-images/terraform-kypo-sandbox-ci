output "pool_url" {
  value       = local.pool_url
  description = "Url to the created KYPO CRP pool."
}
